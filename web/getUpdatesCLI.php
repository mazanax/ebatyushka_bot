#!/usr/bin/env php
<?php
declare(strict_types=1);

$autoload = require __DIR__ . '/../vendor/autoload.php';
$app = new \Ebatyushka\Base\App();

$telegram = new \Ebatyushka\Component\Telegram($app->get('api_key'), $app);
$telegram->handleUpdates();