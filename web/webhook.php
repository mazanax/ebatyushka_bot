<?php
declare(strict_types = 1);

require __DIR__ . '/../vendor/autoload.php';

use Xsolla\SDK\Webhook\WebhookServer;
use Xsolla\SDK\Webhook\Message\Message;
use Xsolla\SDK\Exception\Webhook\XsollaWebhookException;
use Xsolla\SDK\Exception\Webhook\InvalidUserException;

$app = new \Ebatyushka\Base\App();
/** @var \Ebatyushka\Provider\UserProvider $userProvider */
$userProvider = $app->get('provider.user');

$callback = function (Message $message) use ($app, $userProvider) {
    switch ($message->getNotificationType()) {
        case Message::USER_VALIDATION:
            /** @var Xsolla\SDK\Webhook\Message\UserValidationMessage $message */
            $userId = $message->getUserId();

            if (!is_numeric($userId) || !$userProvider->exists((int)$userId)) {
                throw new InvalidUserException;
            }

            break;
        case Message::PAYMENT:
            /** @var \Xsolla\SDK\Webhook\Message\PaymentMessage $message */
            $data = $message->toArray();
            $transaction = $message->getTransaction();
            $customParametersArray = $message->getCustomParameters();

            /** @var \Ebatyushka\Provider\TransactionsProvider $transactionProvider */
            $transactionProvider = $app->get('provider.transaction');
            $dbTransaction = $transactionProvider->findById((int)$customParametersArray['transaction_id']);
            $dbTransaction->setStatus(\Ebatyushka\Model\Transaction::STATUS_PAYED);
            $dbTransaction->setRawData(json_encode(['transaction' => $transaction, 'params' => $customParametersArray]));

            $user = $userProvider->findById((int)$message->getUserId());
            $user->modifyBalance((int)$customParametersArray['count']);
            $userProvider->persist($user);

            $transactionProvider->save($dbTransaction);

            break;
        case Message::REFUND:
            throw new XsollaWebhookException('Refund is not available');
            break;
        default:
            throw new XsollaWebhookException('Notification type not implemented');
    }
};

$webhookServer = WebhookServer::create($callback, $app->get('billing.project_key'));
$webhookServer->start();