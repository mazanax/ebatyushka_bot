<?php
declare(strict_types = 1);

use Ebatyushka\Component\Response;

/**
 * @param int $chatId
 * @return Response
 */
function response(int $chatId): Response
{
    return new Response($chatId);
}

function empty_response(): Response
{
    return new Response(null);
}

function to_camel_case($str, $capitalise_first_char = false)
{
    if ($capitalise_first_char) {
        $str[0] = strtoupper($str[0]);
    }
    return preg_replace_callback('/_([a-z])/', function ($c) {
        return strtoupper($c[1]);
    }, $str);
}