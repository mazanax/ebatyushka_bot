<?php
declare(strict_types = 1);

namespace Ebatyushka\Provider;


use Ebatyushka\Component\Provider;
use Ebatyushka\Model\Transaction;
use Ebatyushka\Model\User;

class TransactionsProvider extends Provider
{
    /**
     * @param User $user
     * @param int $sum
     * @param string $item
     * @return Transaction
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function create(User $user, int $sum, string $item): Transaction
    {
        $this->db->beginTransaction();
        $this->db->insert('"transaction"', [
            'user_id' => $user->getId(),
            'sum' => $sum,
            'item' => $item,
            'status' => Transaction::STATUS_OPEN,
            'created_at' => $date = (new \DateTime())->format(\DateTime::ISO8601),
            'updated_at' => $date,
            'raw_data' => '',
        ]);
        $id = (int)$this->db->lastInsertId();
        $this->db->commit();

        return $this->findById($id);
    }

    /**
     * @param Transaction $transaction
     */
    public function save(Transaction $transaction)
    {
        $this->db->update('"transaction"', [
            'status' => $transaction->getStatus(),
            'updated_at' => (new \DateTime())->format(\DateTime::ISO8601),
            'raw_data' => $transaction->getRawData(),
        ], ['id' => $transaction->getId()]);
    }

    /**
     * @param int $id
     * @return Transaction
     */
    public function findById(int $id): Transaction
    {
        $data = $this->db->fetchAssoc('SELECT * FROM "transaction" WHERE id = ?', [$id]);
        array_walk($data, function (&$value, $key) {
            if (in_array($key, ['id', 'sum', 'user_id'], true)) {
                $value = (int)$value;
            }
        });
        
        return Transaction::create($data);
    }
}
