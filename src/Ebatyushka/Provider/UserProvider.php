<?php
declare(strict_types = 1);

namespace Ebatyushka\Provider;


use Ebatyushka\Component\Provider;
use Ebatyushka\Component\State;
use Ebatyushka\Model\User;

class UserProvider extends Provider
{
    private $defaultBalance = 0;

    /**
     * @param int $balance
     * @return UserProvider
     */
    public function setDefaultBalance(int $balance): UserProvider
    {
        $this->defaultBalance = $balance;

        return $this;
    }

    /**
     * @param int $id
     * @return User
     */
    public function findById(int $id): User
    {
        $data = $this->db->fetchAssoc('SELECT * FROM user WHERE id = ?', [$id]);
        array_walk(
            $data,
            function (&$value, $key) {
                if (in_array($key, ['sex', 'state', 'balance', 'id'], true)) {
                    $value = (int)$value;
                }
            }
        );

        return new User($data);
    }

    /**
     * @param User $user
     */
    public function persist(User $user)
    {
        if ($this->exists($user->getId())) {
            $oldUser = $this->findById($user->getId());
            $arrayUser = $user->toArray();

            $data = [];
            foreach ($oldUser->toArray() as $property => $value) {
                if (!array_key_exists($property, $arrayUser) || $arrayUser[$property] === null) {
                    continue;
                }

                if ($value !== $arrayUser[$property]) {
                    $data[$property] = $arrayUser[$property];
                }
            }
            if (null !== $user->getNewSex()) {
                $data['sex'] = $user->getNewSex();
            }

            if (0 !== count($data)) {
                $this->db->update('user', $data, ['id' => $user->getId()]);
            }
        } else {
            $data = [
                'id' => $user->getId(),
                'first_name' => $user->getFirstName(),
                'last_name' => $user->getLastName(),
                'username' => $user->getUserName(),
                'balance' => $this->defaultBalance,
                'state' => State::STATE_START,
            ];
            
            $this->db->insert('user', $data);
        }
        $user->markAsSaved();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function exists(int $id): bool
    {
        return (bool)$this->db->fetchColumn('select COUNT(1) from user where id = ?', [$id]);
    }

    /**
     * @deprecated use User::setState() instead
     *
     * @param int $id
     * @param int $state
     */
    public function setState(int $id, int $state)
    {
        $this->db->update('user', ['state' => $state], ['id' => $id]);
    }

    /**
     * @deprecated useUser::setBalance() instead
     *
     * @param int $id
     * @param int $balance
     */
    public function setBalance(int $id, int $balance)
    {
        $this->db->update('user', ['balance' => $balance], ['id' => $id]);
    }
}
