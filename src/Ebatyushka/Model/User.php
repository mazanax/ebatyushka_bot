<?php
declare(strict_types = 1);

namespace Ebatyushka\Model;


class User
{
    private $id;
    private $firstName;
    private $lastName;
    private $userName;
    private $state;
    private $balance;
    private $sex;

    private $exists = false;
    private $newSex = null;

    /**
     * User constructor.
     * @param array $params
     * @param bool $exists
     */
    public function __construct(array $params = [], $exists = false)
    {
        foreach ($params as $param => $value) {
            $property = to_camel_case($param);
            if ($param === 'username') {
                $property = 'userName';
            }

            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }

        $this->exists = $exists;
    }

    /**
     * @return User
     */
    public function markAsSaved(): User
    {
        $this->exists = true;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int)$this->id;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return (int)$this->balance;
    }

    /**
     * @param int $balance
     * @return User
     */
    public function setBalance(int $balance): User
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return (int)$this->state;
    }

    /**
     * @param int $state
     * @return User
     */
    public function setState(int $state): User
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasMoney(): bool
    {
        return 0 < $this->getBalance();
    }

    /**
     * @param int $diff
     * @return User
     */
    public function modifyBalance(int $diff): User
    {
        $this->balance += $diff;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccost(): string
    {
        return ($this->newSex ?? $this->sex) === 1 ? 'сын мой' : 'дочь моя';
    }

    /**
     * @return string
     */
    public function getSuffixForVerb(): string
    {
        return ($this->newSex ?? $this->sex) === 1 ? 'ся' : 'ась';
    }

    /**
     * @param mixed $sex
     * @return User
     */
    public function setSex(int $sex): User
    {
        $this->newSex = $sex;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExistsInDb(): bool
    {
        return $this->exists;
    }

    /**
     * @return null
     */
    public function getNewSex()
    {
        return $this->newSex;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'firstName' => $this->firstName,
            'lastName' => $this->lastName,
            'userName' => $this->userName,
            'balance' => $this->balance,
            'state' => $this->state,
        ];
    }
}