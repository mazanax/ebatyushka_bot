<?php
declare(strict_types = 1);

namespace Ebatyushka\Model;


class Transaction
{
    const STATUS_OPEN = 'open';
    const STATUS_PAYED = 'payed';
    const STATUS_FAILED = 'failed';

    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $sum;
    /**
     * @var string
     */
    private $item;
    /**
     * @var int
     */
    private $userId;

    /**
     * @var \DateTime
     */
    private $createdAt;
    /**
     * @var \DateTime
     */
    private $updatedAt;
    /**
     * @var string
     */
    private $status;
    /**
     * @var string
     */
    private $rawData;

    /**
     * Transaction constructor.
     * @param int $userId
     * @param int $sum
     * @param string $item
     */
    public function __construct(int $userId, int $sum, string $item)
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->item = $item;
        $this->sum = $sum;
        $this->userId = $userId;
    }

    /**
     * @param array $params
     * @return Transaction
     */
    public static function create(array $params = []): Transaction
    {
        $transaction = new static($params['user_id'], $params['sum'], $params['item']);
        foreach ($params as $key => $value) {
            $property = to_camel_case($key);
            $setter = 'set' . ucfirst($property);

            if (method_exists($transaction, $setter)) {
                $transaction->$setter($value);
            }
        }

        return $transaction;
    }

    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSum(): int
    {
        return $this->sum;
    }

    /**
     * @return string
     */
    public function getItem(): string
    {
        return $this->item;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @throws \InvalidArgumentException
     */
    public function setStatus($status)
    {
        if (!in_array($status, [static::STATUS_OPEN, static::STATUS_PAYED, static::STATUS_FAILED], true)) {
            throw new \InvalidArgumentException('Invalid status: ' . $status);
        }

        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getRawData(): string
    {
        return $this->rawData;
    }

    /**
     * @param string $rawData
     */
    public function setRawData($rawData = '')
    {
        $this->rawData = $rawData ?? '';
    }

}
