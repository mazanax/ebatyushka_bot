<?php
declare(strict_types = 1);

use Ebatyushka\Command\BuyActionCommand;
use Ebatyushka\Command\BuyMoneyActionCommand;
use Ebatyushka\Command\CancelActionCommand;
use Ebatyushka\Command\CandleActionCommand;
use Ebatyushka\Command\ConfessActionCommand;
use Ebatyushka\Command\DefaultActionCommand;
use Ebatyushka\Command\SexActionCommand;
use Ebatyushka\Command\StartActionCommand;


return [
    '_default' => DefaultActionCommand::class,
    '_aliases' => [
        'У вас \d+🕯' => BuyActionCommand::ROUTE,
        '\d+🕯за \d+₽' => BuyMoneyActionCommand::ROUTE,
        'Отменить' => CancelActionCommand::ROUTE,
        'Вернуться в меню' => CancelActionCommand::ROUTE,
    ],
    StartActionCommand::ROUTE => StartActionCommand::class,
    SexActionCommand::ROUTE_MALE=> [ // male
        'class' => SexActionCommand::class,
        'sex' => 1,
    ],
    SexActionCommand::ROUTE_FEMALE => [ // female
        'class' => SexActionCommand::class,
        'sex' => 2,
    ],
    CandleActionCommand::ROUTE => CandleActionCommand::class,
    ConfessActionCommand::ROUTE => ConfessActionCommand::class,
    BuyActionCommand::ROUTE => BuyActionCommand::class,
    CancelActionCommand::ROUTE => CancelActionCommand::class,
    BuyMoneyActionCommand::ROUTE => BuyMoneyActionCommand::class,
];