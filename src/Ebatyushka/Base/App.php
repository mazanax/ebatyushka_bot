<?php
declare(strict_types=1);

namespace Ebatyushka\Base;


use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;
use Ebatyushka\Provider\TransactionsProvider;
use Ebatyushka\Provider\UserProvider;

class App
{
    private $container = [];
    public $routing = [];

    /**
     * App constructor.
     * @throws \Doctrine\DBAL\DBALException
     * @throws \InvalidArgumentException
     */
    public function __construct()
    {
        $this->container = $this->loadConfig();
        $this->container['db'] = DriverManager::getConnection([
            'path' => $this->get('db.path'),
            'driver' => 'pdo_sqlite'
        ], new Configuration());
        $this->routing = require __DIR__ . '/../config/routing.php';

        $this->container['provider.user'] = function (App $app) {
            return (new UserProvider($app->get('db')))
                ->setDefaultBalance($app->get('defaults.balance'));
        };
        $this->container['provider.transaction'] = function (App $app) {
            return new TransactionsProvider($app->get('db'));
        };
    }

    /**
     * @param $key
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function get($key)
    {
        if (!array_key_exists($key, $this->container)) {
            throw new \InvalidArgumentException("$key is not defined");
        }
        $result = $this->container[$key];
        if (is_callable($result)) {
            $result = $result($this);
            $this->container[$key] = $result;
        }

        return $result;
    }

    /**
     * @return array
     */
    private function loadConfig(): array
    {
        return $this->walkConfig(include __DIR__ . '/../config/default.php');
    }

    /**
     * @param array $config
     * @param string $prefix
     * @return array
     */
    private function walkConfig(array $config, string $prefix = ''): array
    {
        $result = [];
        foreach ($config as $key => &$value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->walkConfig($value, "$key."));
            } else {
                $result[$prefix.$key] = $value;
            }
        }

        return $result;
    }
}
