<?php
declare(strict_types = 1);

namespace Ebatyushka\Component;


/**
 * Class ChatAction
 * @package Ebatyushka\Component
 */
class ChatAction
{
    const TYPING = 'typing';
    const UPLOAD_PHOTO = 'upload_photo';
    const UPLOAD_VIDEO = 'upload_video';
    const UPLOAD_AUDIO = 'upload_audio';
    const UPLOAD_DOCUMENT = 'upload_document';
    const RECORD_VIDEO = 'record_video';
    const RECORD_AUDIO = 'record_audio';
    const FIND_LOCATION = 'find_location';

    const NO_DELAY = 0;

    private $action;
    private $delay;

    /**
     * ChatAction constructor.
     * @param string $action
     * @param int $delay
     * @throws \InvalidArgumentException
     */
    public function __construct(string $action, int $delay)
    {
        if (!in_array($action, $this->getActions(), true)) {
            throw new \InvalidArgumentException('Invalid action: ' . $action);
        }
        if ($delay < 0 || $delay > 5000) {
            throw new \InvalidArgumentException('Invalid delay. Allowed delay in range from 0 to 5000');
        }

        $this->action = $action;
        $this->delay = $delay;
    }

    /**
     * @return array
     */
    private function getActions(): array
    {
        return [
            static::TYPING,
            static::UPLOAD_PHOTO,
            static::UPLOAD_VIDEO,
            static::UPLOAD_AUDIO,
            static::UPLOAD_DOCUMENT,
            static::RECORD_AUDIO,
            static::RECORD_VIDEO,
            static::FIND_LOCATION,
        ];
    }

    public function makeDelay()
    {
        usleep(1000 * $this->delay);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->action;
    }
}
