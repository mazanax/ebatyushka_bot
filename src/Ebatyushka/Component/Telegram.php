<?php
declare(strict_types = 1);

namespace Ebatyushka\Component;


use Doctrine\DBAL\Connection;
use Ebatyushka\Base\App;
use Ebatyushka\Model\User;
use Ebatyushka\Provider\UserProvider;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Teapot\HttpResponse\Status\StatusCode;

class Telegram
{
    const API_URL = 'https://api.telegram.org/bot';
    /**
     * @var Client $client
     */
    public static $client;
    /**
     * @var string
     */
    private $apiKey;
    /**
     * @var Connection
     */
    private $db;
    /**
     * @var App
     */
    private $app;

    /**
     * Telegram constructor.
     * @param string $apiKey
     * @param App $app
     * @throws \InvalidArgumentException
     */
    public function __construct(string $apiKey, App $app)
    {
        $this->apiKey = $apiKey;
        $this->app = $app;
        $this->db = $app->get('db');
    }

    public function handleUpdates()
    {
        $updates = $this->getUpdates();

        foreach ($updates as $update) {
            if (!array_key_exists('message', $update)) {
                continue;
            }
            $from = $update['message']['from'];

            $user = $this->checkUserAndInsert(
                $from['id'],
                $from['first_name'] ?? '',
                $from['last_name'] ?? '',
                $from['username'] ?? ''
            );
            $data = [
                'id' => $update['update_id'],
                'text' => $update['message']['text'],
                'created_at' => $update['message']['date'],
                'user_id' => $from['id'],
                'chat_id' => $update['message']['chat']['id'],
                'raw' => json_encode($update),
            ];
            $this->db->insert('"update"', $data);

            $route = $update['message']['text'];
            $class = $this->resolveCommand(
                $route,
                [
                    'user' => $user,
                    'chat_id' => $update['message']['chat']['id'],
                    'text' => $update['message']['text'],
                ]
            );
            $response = $class->execute();

            $this->sendResponse($response);
            $this->app->get('provider.user')->persist($user);
        }
    }

    /**
     * @param string $route
     * @param array $params
     * @return AbstractCommand
     */
    private function resolveCommand(string $route, array $params): AbstractCommand
    {
        $_route = null;
        /** @var array $routeCollection */
        $routeCollection = $this->app->routing;

        if (array_key_exists($route, $routeCollection)) {
            $_route = $routeCollection[$route];
        } else {
            /** @var string[] $aliases */
            $aliases = $routeCollection['_aliases'];
            foreach ($aliases as $pattern => $routeData) {
                if (preg_match('/' . $pattern . '/i', $route)) {
                    $_route = $routeCollection[$routeData];
                    break;
                }
            }
        }
        if (null !== $_route) {
            $className = is_string($_route) ? $_route : $_route['class'];
            if (is_array($_route)) {
                $r = $_route;
                unset($r['class']);

                $params = array_merge($params, $r);
            }
        } else {
            $className = $routeCollection['_default'];
        }

        /** @var AbstractCommand $class */
        return new $className($this->app, $params);
    }

    /**
     * @return int
     */
    private function getLastUpdate(): int
    {
        return (int)$this->db->fetchColumn('select MAX(id) from "update"');
    }

    /**
     * @return array
     * @throws \RuntimeException
     */
    private function getUpdates(): array
    {
        $request = static::makeApiRequest(
            static::API_URL . $this->apiKey . '/getUpdates?offset=' . ($this->getLastUpdate() + 1)
        );

        if (StatusCode::OK === $request->getStatusCode()) {
            $response = json_decode($request->getBody()->getContents(), true);

            if (array_key_exists('ok', $response) && $response['ok']) {
                return $response['result'];
            }
        }

        return [];
    }

    /**
     * @param int $id
     * @param string $firstName
     * @param string $lastName
     * @param string $userName
     * @return User
     * @throws \InvalidArgumentException
     */
    private function checkUserAndInsert(
        int $id,
        string $firstName = '',
        string $lastName = '',
        string $userName = ''
    ): User
    {
        /** @var UserProvider $userProvider */
        $userProvider = $this->app->get('provider.user');
        $userProvider->persist(
            $user = new User(['id' => $id, 'firstName' => $firstName, 'lastName' => $lastName, 'userName' => $userName])
        );

        return $userProvider->findById($id);
    }

    /**
     * @param Response $response
     */
    private function sendResponse(Response $response)
    {
        $content = $response->getContent();
        $data = [
            'chat_id' => $response->getChatId(),
        ];
        if ($response->getKeyboard()) {
            $data['reply_markup'] = [
                $response->withInlineKeyboard() ? 'inline_keyboard' : 'keyboard' => $response->getKeyboard(),
                'resize_keyboard' => true,
            ];
            if (!$response->withInlineKeyboard()) {
                $data['reply_markup']['resize_keyboard'] = true;
            }
            $data['reply_markup'] = json_encode($data['reply_markup']);
        } else {
            $data['reply_markup'] = json_encode(['hide_keyboard' => true]);
        }
        if ($response->withMarkdown()) {
            $data['parse_mode'] = 'Markdown';
            $data['disable_web_page_preview'] = true;
        }
        if (0 !== $response->getActions()) {
            $this->processActions($response->getChatId(), $response->getActions());
        }
        $data['text'] = is_callable($content) ? call_user_func($content) : $content;

        static::makeApiRequest(static::API_URL . $this->apiKey . '/sendMessage', $data, 'POST');
    }

    /**
     * @param int $chatId
     * @param ChatAction[] $actions
     */
    private function processActions(int $chatId, array $actions)
    {
        foreach ($actions as $action) {
            $this->sendAction($chatId, $action);
        }
    }

    /**
     * @param int $chatId
     * @param ChatAction $action
     */
    private function sendAction(int $chatId, ChatAction $action)
    {
        static::makeApiRequest(
            static::API_URL . $this->apiKey . '/sendChatAction',
            [
                'chat_id' => $chatId,
                'action' => (string)$action,
            ],
            'POST'
        );

        $action->makeDelay();
    }

    /**
     * @param string $url
     * @param array $formData
     * @param string $method
     * @return ResponseInterface
     */
    public static function makeApiRequest(string $url, array $formData = [], string $method = 'GET'): ResponseInterface
    {
        if (!static::$client) {
            static::$client = new Client();
        }
        $formData = ['form_params' => $formData];

        return static::$client->request($method, $url, $formData);
    }
}
