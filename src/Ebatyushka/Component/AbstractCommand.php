<?php
declare(strict_types = 1);

namespace Ebatyushka\Component;


use Ebatyushka\Base\App;
use Ebatyushka\Component\Gettable;
use Ebatyushka\Model\User;

/**
 * Class AbstractCommand
 * @package Ebatyushka\Component
 */
abstract class AbstractCommand
{
    private $app;
    /** @var Gettable $request */
    protected $request;
    protected $rawRequest;
    /** @var User $user */
    protected $user;
    /** @var int */
    protected $chatId;

    /**
     * AbstractCommand constructor.
     * @param App $app
     * @param array $request
     */
    public function __construct(App $app, array $request = [])
    {
        $this->app = $app;
        $this->rawRequest = $request;
        $this->request = new class($request) implements Gettable {
            private $container;

            public function __construct(array $container = [])
            {
                $this->container = $container;
            }

            public function get(string $key)
            {
                return $this->container[$key] ?? null;
            }
        };

        $this->user = &$request['user'];
        $this->chatId = $request['chat_id'];
    }

    public function after()
    {
        
    }

    /**
     * @param $key
     * @return mixed
     * @throws \InvalidArgumentException
     */
    protected function get(string $key)
    {
        return $this->app->get($key);
    }

    /**
     * @param $method
     */
    protected function track(string $method)
    {
        /* @TODO refactor this shit */
        if (false === true) {
            file_get_contents(
                'https://api.botan.io/track?token=' . $this->get('metrica.token')
                . '&uid=' . $this->user->getId()
                . '&name=' . $method
            );
        }
    }

    /**
     * @return Response
     */
    abstract public function execute(): Response;
}
