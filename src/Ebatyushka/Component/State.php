<?php
declare(strict_types = 1);

namespace Ebatyushka\Component;

use Ebatyushka\Command\BuyActionCommand;
use Ebatyushka\Command\CancelActionCommand;
use Ebatyushka\Command\CandleActionCommand;
use Ebatyushka\Command\ConfessActionCommand;
use Ebatyushka\Command\SexActionCommand;

class State
{
    const STATE_START = 0;
    const STATE_MAIN = 1;
    const STATE_BALANCE = 2;
    const STATE_CONFESS = 3;
    const STATE_NO_MONEY = -1;

    public static function getKeyboard(int $state, int $balance = 0): array
    {
        $keyboards = static::getKeyboards($balance);
        return $keyboards[$state];
    }
    
    private static function getKeyboards(int $balance = 0): array
    {
        return [
            static::STATE_START => [[SexActionCommand::ROUTE_MALE, SexActionCommand::ROUTE_FEMALE]],
            static::STATE_MAIN => [
                ['У вас ' . $balance . '🕯'],
                [CandleActionCommand::ROUTE],
                [ConfessActionCommand::ROUTE],
            ],
            static::STATE_NO_MONEY => [[BuyActionCommand::ROUTE], [CancelActionCommand::ROUTE]],
            static::STATE_BALANCE => [
                ['15🕯за 50₽', '40🕯за 100₽'],
                ['100🕯за 150₽', 'Отменить'],
            ],
        ];
    }
}
