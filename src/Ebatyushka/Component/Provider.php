<?php
declare(strict_types = 1);

namespace Ebatyushka\Component;


use Doctrine\DBAL\Connection;

class Provider
{
    /**
     * @var Connection
     */
    protected $db;

    /**
     * Provider constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }
}
