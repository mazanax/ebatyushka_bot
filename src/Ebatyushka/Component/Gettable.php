<?php
declare(strict_types = 1);

namespace Ebatyushka\Component;


interface Gettable
{
    public function get(string $key);
}