<?php
declare(strict_types = 1);

namespace Ebatyushka\Component;

/**
 * Class Response
 * @package Ebatyushka\Component
 */
class Response
{
    /** @var int */
    private $_chatId;
    /** @var string|callable */
    private $_content;
    /** @var bool */
    private $_markdown = false;
    /** @var array */
    private $_keyboard;
    /** @var bool */
    private $_inlineKeyboard = false;
    /** @var ChatAction[] */
    private $_actions = [];

    /**
     * Response constructor.
     * @param $chatId
     */
    public function __construct($chatId)
    {
        $this->_chatId = null !== $chatId ? (int)$chatId : $chatId;
    }

    /**
     * @param string|callable $data
     * @param bool $markdown
     * @return $this|Response
     */
    public function content($data, bool $markdown = false): Response
    {
        $this->_content = $data;
        $this->_markdown = $markdown;

        return $this;
    }

    /**
     * @param string|callable $data
     * @param bool $markdown
     * @return Response
     */
    public function text(string $data, bool $markdown = false): Response
    {
        return $this->content($data, $markdown);
    }

    /**
     * @param array $keyboard
     * @param bool $inline
     * @return Response
     */
    public function keyboard(array $keyboard = [], bool $inline = false): Response
    {
        $this->_keyboard = 0 === count($keyboard) ? null : $keyboard;
        $this->_inlineKeyboard = $inline;

        return $this;
    }

    /**
     * @param string $action
     * @param int $delay
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function withAction(string $action, int $delay = 5000): Response
    {
        $this->_actions[] = new ChatAction($action, $delay);

        return $this;
    }

    /**
     * @return int
     */
    public function getChatId(): int
    {
        return $this->_chatId;
    }

    /**
     * @return string|callable
     */
    public function getContent()
    {
        return $this->_content;
    }

    /**
     * @return boolean
     */
    public function withMarkdown(): bool
    {
        return $this->_markdown;
    }

    /**
     * @return array|null
     */
    public function getKeyboard()
    {
        return $this->_keyboard;
    }

    /**
     * @return boolean
     */
    public function withInlineKeyboard(): bool
    {
        return $this->_inlineKeyboard;
    }

    /**
     * @return ChatAction[]
     */
    public function getActions(): array
    {
        return $this->_actions;
    }

    /**
     * @return bool
     */
    public function isEmpty(): bool
    {
        return null === $this->_chatId;
    }
}
