<?php
declare(strict_types = 1);

namespace Ebatyushka\Command;


use Ebatyushka\Component\AbstractCommand;
use Ebatyushka\Component\Response;
use Ebatyushka\Component\State;

class DefaultActionCommand extends AbstractCommand
{
    /**
     * @return Response
     */
    public function execute(): Response
    {
        $user = $this->user;
        if ($user->getState() !== State::STATE_CONFESS) {
            return empty_response();
        }
        $user->setState(State::STATE_MAIN);

        $random = random_int(1, 100);
        if ($random >= 15 && $random < 35) {
            return response($this->chatId)
                ->text('Прости, ' . $user->getAccost()
                    . ", но ни Иисус, ни Христос не простили тебя в этот раз.\n"
                    . 'Ты недостаточно раскаял' . $user->getSuffixForVerb() . '.')
                ->keyboard(State::getKeyboard(State::STATE_MAIN, $user->getBalance()));
        }
        return response($this->chatId)
            ->text('Я отпускаю твои грехи, ' . $user->getAccost())
            ->keyboard(State::getKeyboard(State::STATE_MAIN, $user->getBalance()));
    }
}
