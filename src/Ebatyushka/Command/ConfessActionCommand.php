<?php
declare(strict_types = 1);

namespace Ebatyushka\Command;


use Ebatyushka\Component\AbstractCommand;
use Ebatyushka\Component\Response;
use Ebatyushka\Component\State;
use Ebatyushka\Model\User;

class ConfessActionCommand extends AbstractCommand
{
    const ROUTE = 'Исповедаться 5🕯';

    /**
     * @return Response
     */
    public function execute(): Response
    {
        /** @var User $user */
        $user = $this->user;
        if ($user->getState() !== State::STATE_MAIN) {
            return empty_response();
        }

        if ($this->user->getBalance() < 5) {
            $this->user->setState(State::STATE_NO_MONEY);

            return response($this->chatId)
                ->text(sprintf("Прости, %s, но Господь не прощает тех,\n"
                   . "кто не жертвует.\n"
                   . 'Купи свечки и я смогу отпустить твои грехи.', $user->getAccost()))
                ->keyboard(State::getKeyboard($user->getState(), 0));
        }
        $this->user->setState(State::STATE_CONFESS)
            ->modifyBalance(-5);

        $this->track('confess');
        return response($this->chatId)
            ->text('Расскажи мне суть греха, ' . $user->getAccost())
            ->keyboard([]);
    }
}
