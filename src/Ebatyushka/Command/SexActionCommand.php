<?php
declare(strict_types = 1);

namespace Ebatyushka\Command;

use Ebatyushka\Component\AbstractCommand;
use Ebatyushka\Component\Response;
use Ebatyushka\Component\State;

/**
 * Class SexActionCommand
 * @package Ebatyushka\Command
 */
class SexActionCommand extends AbstractCommand
{
    const ROUTE_FEMALE = '👩🏻';
    const ROUTE_MALE = '👨🏻';
    
    /**
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function execute(): Response
    {
        $user = $this->user;
        if ($user->getState() !== State::STATE_START) {
            return empty_response();
        }

        $user->setSex((int)$this->request->get('sex'))
            ->setState(State::STATE_MAIN);

        return response($this->chatId)
            ->text(sprintf(
                "Добро пожаловать в храм Божий, %s.\nЧем я могу помочь?",
                $user->getAccost()
            ))
            ->keyboard(State::getKeyboard($user->getState(), $user->getBalance()));
    }
}
