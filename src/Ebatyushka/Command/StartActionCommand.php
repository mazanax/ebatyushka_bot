<?php
declare(strict_types = 1);

namespace Ebatyushka\Command;


use Ebatyushka\Component\AbstractCommand;
use Ebatyushka\Component\Response;
use Ebatyushka\Component\State;

class StartActionCommand extends AbstractCommand
{
    const ROUTE = '/start';

    /**
     * @return Response
     */
    public function execute(): Response
    {
        $user = $this->user;
        if ($user->getState() === State::STATE_CONFESS) {
            return empty_response();
        }

        $user->setState(State::STATE_START);
        $this->track('start');
        
        return response($this->chatId)
            ->text("Добро пожаловать, раб Божий.\nУкажите пол.")
            ->keyboard(State::getKeyboard(State::STATE_START, 0));
    }
}
