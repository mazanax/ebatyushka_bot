<?php
declare(strict_types = 1);

namespace Ebatyushka\Command;


use Ebatyushka\Component\AbstractCommand;
use Ebatyushka\Component\Response;
use Ebatyushka\Component\State;
use Ebatyushka\Model\User;
use Xsolla\SDK\API\PaymentUI\TokenRequest;

class BuyActionCommand extends AbstractCommand
{
    const ROUTE = 'Купить свечки';

    /**
     * @return Response
     */
    public function execute(): Response
    {
        $user = $this->user;
        if (!in_array($user->getState(), [State::STATE_MAIN, State::STATE_NO_MONEY], true)) {
            return empty_response();
        }
        if (!$user->hasMoney()) {
            $user->setState(State::STATE_BALANCE);
        }

        return response($this->chatId)
            ->text('Сколько 🕯 ты хочешь купить, ' . $user->getAccost() . '?')
            ->keyboard(State::getKeyboard(State::STATE_BALANCE, $user->getBalance()));
    }
}
