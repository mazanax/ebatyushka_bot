<?php
declare(strict_types = 1);

namespace Ebatyushka\Command;


use Ebatyushka\Component\AbstractCommand;
use Ebatyushka\Component\Response;
use Ebatyushka\Component\State;
use Ebatyushka\Model\User;

class CancelActionCommand extends AbstractCommand
{
    const ROUTE = 'Отмена';

    /**
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function execute(): Response
    {
        /** @var User $user */
        $user = $this->user;
        if (!$user->hasMoney()
            || in_array($user->getState(),
                [State::STATE_START, State::STATE_CONFESS, State::STATE_BALANCE], true)) {
            return $user->hasMoney() ? empty_response()
                : response($this->chatId)
                    ->text('Сперва купи свечки, ' . $user->getAccost() . '.')
                    ->keyboard(State::getKeyboard($user->getState(), $user->getBalance()));
        }
        $user->setState(State::STATE_MAIN);

        return response($this->chatId)
            ->text('Действие отменено')
            ->keyboard(State::getKeyboard($user->getState(), $user->getBalance()));
    }
}
