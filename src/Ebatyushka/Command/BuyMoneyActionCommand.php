<?php
declare(strict_types = 1);

namespace Ebatyushka\Command;


use Ebatyushka\Component\AbstractCommand;
use Ebatyushka\Component\ChatAction;
use Ebatyushka\Component\Response;
use Ebatyushka\Component\State;
use Ebatyushka\Provider\TransactionsProvider;
use Xsolla\SDK\API\PaymentUI\TokenRequest;
use Xsolla\SDK\API\XsollaClient;

class BuyMoneyActionCommand extends AbstractCommand
{
    const ROUTE = '/buy_money';

    /**
     * @return Response
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \InvalidArgumentException
     */
    public function execute(): Response
    {
        switch ($text = $this->request->get('text')) {
            default:
            case '15🕯за 50₽':
                $count = 15;
                $purchase = 50;
                break;

            case '40🕯за 100₽':
                $count = 40;
                $purchase = 100;
                break;

            case '100🕯за 150₽':
                $count = 100;
                $purchase = 150;
                break;
        }
        $user = $this->user;
        /** @var TransactionsProvider $transactionProvider */
        $transactionProvider = $this->get('provider.transaction');
        $transaction = $transactionProvider->create($user, $purchase, $text);

        return response($this->chatId)
            ->withAction(ChatAction::TYPING, ChatAction::NO_DELAY)
            ->content(function () use ($user, $purchase, $count, $transaction) {
                $tokenRequest = new TokenRequest($this->get('billing.project_id'), '' . $user->getId());
                $tokenRequest->setUserEmail('username@example.com')
                    ->setPurchase($purchase, 'RUB')
                    ->setUserName($this->user->getUserName())
                    ->setCustomParameters(['transaction_id' => $transaction->getId(), 'count' => $count]);
                $xsollaClient = XsollaClient::factory(array(
                    'merchant_id' => $this->get('billing.user_id'),
                    'api_key' => $this->get('billing.api_key'),
                ));
                $token = $xsollaClient->createPaymentUITokenFromRequest($tokenRequest);
                $link = 'https://secure.xsolla.com/paystation2/?access_token=' . $token;

                return 'Для оплаты счета перейди по следующей ссылке, ' . $user->getAccost() . ":\n\n$link";
            }, true)
            ->keyboard([['Вернуться в меню']]);
    }
}
