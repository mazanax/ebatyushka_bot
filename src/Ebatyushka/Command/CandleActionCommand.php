<?php
declare(strict_types = 1);

namespace Ebatyushka\Command;


use Ebatyushka\Component\AbstractCommand;
use Ebatyushka\Component\ChatAction;
use Ebatyushka\Component\Response;
use Ebatyushka\Component\State;
use Ebatyushka\Model\User;

class CandleActionCommand extends AbstractCommand
{
    const ROUTE = 'Поставить свечку 1🕯';

    /**
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function execute(): Response
    {
        /** @var User $user */
        $user = $this->user;
        if ($user->getState() !== State::STATE_MAIN) {
            return empty_response();
        }

        if (!$user->hasMoney()) {
            $user->setState(State::STATE_NO_MONEY);

            return response($this->chatId)
                ->text(sprintf("Прости, %s, но ты не можешь поставить то, чего у тебя нет.\n"
                    . 'Купи свечки и я буду молиться за тебя', $user->getAccost()))
                ->keyboard(State::getKeyboard($user->getState(), $user->getBalance()));
        } else {
            $user->modifyBalance(-1);
            $this->track('candle');

            return response($this->chatId)
                ->withAction(ChatAction::TYPING, ChatAction::NO_DELAY)
                ->content(function () {
                    do {
                        $bible = json_decode(file_get_contents('http://allbible.info/ajax/randomajaxverse/'), true);
                    } while (empty($bible['number']));

                    return sprintf(
                        "`%s`\n(%s %d:%d)",
                        $bible['verse'],
                        $bible['book_ru'],
                        $bible['chapter'],
                        $bible['number']
                    );
                }, true)
                ->keyboard(State::getKeyboard($user->getState(), $user->getBalance()));
        }
    }
}
